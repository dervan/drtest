import re
import sys

f = open(sys.argv[1])
s = f.read()
enums = re.compile(r"E[A-Z][A-Za-z0-9]*[a-z][A-Za-z0-9]*")
structs = re.compile(r"S[A-Z][A-Za-z0-9]*[a-z][A-Za-z_0-9]*")
types = re.compile(r"T[A-Z][A-Za-z0-9]*[a-z][A-Za-z_0-9]*")
print("typedef int u32;\n\
       typedef int i32;\n\
       typedef int i16;\n\
       typedef int u16;")

m_enum = list(set(map(lambda x: x, re.findall(enums,s))))
m_enum.sort()
#print(m_enum)
for e in m_enum:
    e = e.strip(" (")
    print("typedef void "+e+";")
#    if len(list(filter(lambda x: x.startswith(e), m_enum)))>1:
#        print("!!!",e)
m_structs = set(map(lambda x: x, re.findall(structs,s)))
m_types = set(map(lambda x: x, re.findall(types,s)))
for t in m_structs:
    t = t.strip(" (")
    print("typedef void "+t+";")

for t in m_types:
    t = t.strip(" (")
    print("typedef int "+t+";")
