from copy import deepcopy
from pycparser import parse_file, c_parser,c_ast, c_generator

# Useful when working with pycparser
def dump(obj):
    print("%%%%%%%%%%")
    for attr in dir(obj):
        if not attr.startswith("__"):
            print("obj.%s = %s"%( attr, getattr(obj, attr)))
    print("^^^^^^^^^^")


class Condition:
    def __init__(self, neg, cond):
        self.neg = neg 
        self.cond = cond

    def __str__(self):
        generator = c_generator.CGenerator()
        if self.neg!=0:
            return "~"+generator.visit(self.cond)
        else:
            return generator.visit(self.cond)

    def __repr__(self):
        generator = c_generator.CGenerator()
        if self.neg!=0:
            return "~"+generator.visit(self.cond)
        else:
            return generator.visit(self.cond)

class Return:
    def __init__(self,val):
        self.val = val
    def __repr__(self):
        generator = c_generator.CGenerator()
        s = "CHECK_EQUAL("+generator.visit(self.val)+", result)"
        return s
    def __str__(self):
        generator = c_generator.CGenerator()
        s = "CHECK_EQUAL("+generator.visit(self.val)+", result)"
        return s

class ReturnVisitor(c_ast.NodeVisitor):
    def __init__(self):
        self.foundValue = None
    def visit_Return(self,node):
        self.foundValue = node.expr

class IDVisitor(c_ast.NodeVisitor):
    name = ""
    def visit_IdentifierType(self,node):
        self.name = node.names[0]

class ReturnTokenVisitor(c_ast.NodeVisitor):
    def __init__(self,returnToken):
        self.foundValue = None
        self.returnToken = returnToken

    def visit_Assignment(self,node):
        if node.lvalue!=None:
            if type(node.lvalue) is c_ast.ID:
                if(node.lvalue.name==self.returnToken):
                    self.foundValue = node.rvalue

class ParamVisitor(c_ast.NodeVisitor):
    def __init__(self):
        self.names = []
        self.types = []
    def visit_PtrDecl(self,node):
        i = IDVisitor()
        i.visit(node)
        self.types.append(i.name+" *")
        p = ParamVisitor()
        p.visit(node.type)
        self.names.append("&"+p.names[0])
    def visit_TypeDecl(self,node):
        self.names.append(str(node.declname))
        self.types.append(str(node.type.names[0]))

class ParamDeclVisitor(c_ast.NodeVisitor):
    def __init__(self):
        self.result = ""
        self.generator = c_generator.CGenerator()
    def visit_Decl(self,node):
        try:
            node.type.type.quals=None # Remove const in pointer
        except:
            pass
        try:
            node.type.quals=None # Remove const
        except:
            pass
        if isinstance(node.type,c_ast.PtrDecl):
            #node.type = node.type.type
            node_copy = deepcopy(node)
            node_copy.type = node_copy.type.type
            self.result+="    "+self.generator.visit(node_copy)+" = 0;\n"
        else:
            self.result+="    "+self.generator.visit(node)+" = 0;\n"

class IfVisitor(c_ast.NodeVisitor):
    def __init__(self,returnToken=None):
        self.returnToken = returnToken
        self.condList = []
        self.visitedIfs = 0
        self.testCases = []
        self.generator = c_generator.CGenerator()
        c_ast.NodeVisitor.__init__(self);
    def visit_If(self,node):
        self.visitedIfs+=1
        self.condList.append(Condition(0,node.cond))
        oldVisited = self.visitedIfs
        self.visit(node.iftrue)
        if oldVisited == self.visitedIfs:
            if self.returnToken == None:
                r = ReturnVisitor()
            else:
                r = ReturnTokenVisitor(self.returnToken)
            r.visit(node.iftrue)
            if (r.foundValue!=None):
                self.condList.append(Return(r.foundValue))
                self.testCases.append(deepcopy(self.condList))
                self.condList.pop()
            else:
                self.testCases.append(deepcopy(self.condList))
        self.condList.pop()
        if(node.iffalse != None):
            self.condList.append(Condition(1,node.cond))
            oldVisited = self.visitedIfs
            self.visit(node.iffalse)
            if oldVisited == self.visitedIfs:
                if self.returnToken == None:
                    r = ReturnVisitor()
                else:
                    r = ReturnTokenVisitor(self.returnToken)
                r.visit(node.iffalse)
                if (r.foundValue!=None):
                    self.condList.append(Return(r.foundValue))
                    self.testCases.append(deepcopy(self.condList))
                    self.condList.pop()
                else:
                    self.testCases.append(deepcopy(self.condList))
            self.condList.pop()
        else:
            self.condList.append(Condition(1,node.cond))
            self.testCases.append(deepcopy(self.condList))
            self.condList.pop()

class FuncCallVisitor(c_ast.NodeVisitor):
    def __init__(self):
        self.calls = []
    def visit_FuncCall(self, node):
        self.calls.append(node.name.name)

class DependenciesBuilder(c_ast.NodeVisitor):
    def __init__(self):
        self.functions = []
        self.dependencies = []
    def visit_FuncDef(self, node):
        self.functions.append(node.decl.name)
        fcv = FuncCallVisitor()
        fcv.visit(node.body)
        self.dependencies.append((node.decl.name,fcv.calls))

