from __future__ import print_function
import sys
from copy import deepcopy
import argparse

# This is not required if you've installed pycparser into
# your site-packages/ with setup.py
#
sys.path.extend(['.', '..'])

from pycparser import parse_file, c_parser,c_ast, c_generator
from collections import Counter
from UtGenerator import *

class TestSetInfo:
    def __init__(self):
        self.dependencies = {}
        self.functions = []
        self.functionNodes = {}
        self.external = []
        self.statics = []
        self.buckets = {}
        self.testCases = []
        self.header = ""
        self.testGroup = ""
        self.ast = None

    def getExternal(self):
        flatUniqueDeps = list(set([dep for singleDepList in self.dependencies for dep in singleDepList[1]]))
        self.external = list(filter(lambda x: not x in self.functions, flatUniqueDeps))

    def divide(self):
        internal = [list(filter(lambda x: x in self.functions,dep[1])) for dep in self.dependencies]
        internalConnections = dict([(self.dependencies[x][0],set(internal[x])) for x in range(len(self.dependencies))])
        flatten = [t for k in internal for t in k]
        rank = (Counter(flatten)).most_common()
        mainBucket = list(filter(lambda x: x not in flatten,self.functions))
        buckets = []
        for f in rank:
            func = f[0]
            matched = False
            for b in buckets:
                usedInBucket = False
                for bf in b:
                    if func in internalConnections[bf]:
                        usedInBucket = True
                        break
                    if bf in internalConnections[func]:
                        usedInBucket = True
                        break
                if usedInBucket == False:
                    matched = True
                    b.append(func)
                    break
            if matched == False:
                buckets.append([func])
        buckets.insert(0,mainBucket)
        self.buckets = buckets

def generateFake(funcDecl):
    p = ParamVisitor()
    if funcDecl.type.args is not None:
        p.visit(funcDecl.type.args)
    else:
        p.visit(funcDecl.type)
    typesList = ", ".join(p.types)
    t = IDVisitor()
    t.visit(funcDecl.type)
    returnedType = t.name
    if returnedType == None or returnedType == 'void':
        return "FAKE_VOID_FUNC("+funcDecl.name+", "+", ".join(p.types)+")\n"
    return "FAKE_VALUE_FUNC("+returnedType+", "+funcDecl.name+", "+typesList+")\n"

class UtSuite:
    def __init__(self,filename,groupname):
        self.filename = filename
        self.groupname = groupname
        self.parsedInfo = None
        self.generator = c_generator.CGenerator()
        self.parseFile()
    """
    Printing stuff
    """
    def printAST(self):
        self.parsedInfo.ast.show()

    def printFakesForFunction(self,function):
        print(self.parsedInfo.testCases[function].replace("\t"," "*4))
    
    def printDivision(self):
        for i,listOfFunctions in enumerate(self.parsedInfo.buckets):
            print("In file UT_"+self.groupname+"_"+str(i)+".cpp")
            for f in listOfFunctions:
                print(f)
    """
    Test environment stuff
    """
    def testGroupString(self,functionsToFake):
        str = "TEST_GROUP(UT_"+self.groupname+")\n{\n"
        str +="\tvoid setup()\n\t{\n"
        for function in functionsToFake:
            str += "\t\tRESET_FAKE("+function+");\n"
        
        str+= "\t}\n\n"
        str +="\tvoid teardown()\n\t{\n\t}\n};\n\n"
        return str

    def getExternalFakes(self):
        return "\n" + str((self.parsedInfo.external)) + "\n"
        fakes = ""
        for e in self.parsedInfo.external:
            fakes+="FAKE_VOID_FUNC("+e+")\n"
        return 

    def fakeFile(self):
        f = open("CompleteFakesFrom"+self.groupname+".cpp",'w')
        for function in self.parsedInfo.functionNodes.values():
                f.write(generateFake(function).replace("\t"," "*4))
        f.close()

    def save(self):
        tabwidth = 4
        """
        Save common fakes
        """
        print("Saving file UT_FakesFor"+self.groupname+".cpp")
        with open("UT_FakesFor"+self.groupname+".cpp",'w') as f:
            f.write(self.getExternalFakes().replace("\t"," "*tabwidth))
        
        """
        Prepare data to computing internal fakes
        """
        internal = [list(filter(lambda x: x in self.parsedInfo.functions,dep[1])) for dep in self.parsedInfo.dependencies]
        internalConnections = dict([(self.parsedInfo.dependencies[x][0],set(internal[x])) for x in range(len(self.parsedInfo.dependencies))])
        
        """
        Iterate over buckets = separate files
        """
        for i,listOfFunctions in enumerate(self.parsedInfo.buckets):
            print("Saving file UT_"+self.groupname+"_"+str(i)+".h")
            functionsToFake = set()
            # Basically used functions from other buckets
            for function in listOfFunctions:
                functionsToFake = functionsToFake.union(set(internalConnections[function]))
            with open("UT_"+self.groupname+"_"+str(i)+".h",'w') as f:
                # Write header
                # Static functions are not mentioned in headers
                f.write("#include <fff.h>\nDEFINE_FFF_GLOBALS\n#define UT_HEADER\n#include \"glo_def.h\"\n\n#undef UT_HEADER")
                f.write("#include \"UT_FakesFor"+self.groupname+".cpp\"")


                for function in listOfFunctions:
                    if function in self.parsedInfo.statics.keys():
                        f.write(self.parsedInfo.statics[function].replace("\t"," "*tabwidth))
                for ftf in functionsToFake:
                    f.write(generateFake(self.parsedInfo.functionNodes[ftf]).replace("\t"," "*tabwidth))
            print("Saving file UT_"+self.groupname+"_"+str(i)+".cpp")
            with open("UT_"+self.groupname+"_"+str(i)+".cpp",'w') as f:
                f.write("extern \"C\" {\n")
                f.write("#include \"UT_"+self.groupname+"_"+str(i)+".h\"\n")
                f.write("}\n\n")
                f.write("#include \"CppUTest/TestHarness.h\"")
                # Write tescases
                f.write(self.testGroupString(functionsToFake).replace("\t"," "*tabwidth))
                for function in listOfFunctions:
                    f.write(self.parsedInfo.testCases[function].replace("\t"," "*tabwidth))

    def parseFile(self):
        parser = c_parser.CParser()
        result = TestSetInfo()
        try:
            ast = parse_file(self.filename,use_cpp=True)
            result.ast = ast
            #Build dependencies, prepere list of fakes and get division to multiple files
            d = DependenciesBuilder()
            d.visit(ast)
            result.dependencies = d.dependencies
            result.functions = d.functions
            result.getExternal() # getExternal(d.functions,list(map(lambda x: x[1],d.dependencies)))
            result.divide() # = divide(d.functions,d.dependencies)
            result.testGroup = self.groupname
            #Generate all UT cases
            v = UtFunctionVisitor(self.groupname)
            v.visit(ast)
            result.testCases = v.testCases
            result.statics = v.statics
            result.functionNodes = v.functionNodes
            self.parsedInfo = result
        except c_parser.ParseError:
            e = sys.exc_info()[1]
            print("Parse error:" + str(e))
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Unit test generator')
    parser.add_argument('-f',required=True, help="Source file")
    parser.add_argument('-g',required=False,default=None, help="Test group name")
    parser.add_argument('-u',required=False, help="Print tests for given function")
    parser.add_argument('-p',dest='printAst',action='store_true', help="Print AST")
    parser.add_argument('-d',dest='printDivision',action='store_true', help ="Print division between independent files")
    parser.add_argument('-F',dest='fakeFile',action='store_true',help="Generate fakes for every function in given file")
    parser.set_defaults(printAst=False)
    parser.set_defaults(fakeFile=False)
    parser.set_defaults(printDivision=False)
    args = parser.parse_args()
    if args.g == None:
        args.g = max(args.f.split("."), key=len).replace('/','')
    us = UtSuite(args.f,args.g)

    if args.fakeFile:
        us.fakeFile()
    elif args.printDivision:
        us.printDivision()
    elif args.printAst:
        us.printAST()
    elif args.u != None:
        us.printFakesForFunction(args.u)
    else:
        us.save()
