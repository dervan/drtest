from pycparser import parse_file, c_parser,c_ast, c_generator
from visitors import *

class UtFunctionVisitor(c_ast.NodeVisitor):
    def __init__(self, testGroup):
        self.testCases = {}
        self.statics = {}
        self.testGroup = testGroup
        self.functionNodes = {}
        self.generator = c_generator.CGenerator()
    def visit_FuncDef(self, node):
        print('Processing function  %s at %s' % (node.decl.name, node.decl.coord))
        if node.decl.storage != []:
            node.decl.storage = []
            self.statics[node.decl.name] = self.generator.visit(node.decl)+";\n";
        self.functionNodes[node.decl.name] = node.decl
        ut = UtGenerator(node,self.testGroup)
        self.testCases[node.decl.name] = ut.generate()

class UtGenerator:
    def __init__(self,function, testGroup):
        self.function = function
        self.params = function.decl.type.args
        self.body = function.body
        self.testGroup = testGroup
        if type(self.body.block_items[-1]) is c_ast.Return and type(self.body.block_items[-1].expr) == c_ast.ID:
            self.returnToken = self.body.block_items[-1].expr.name;
        else:
            self.returnToken = None
    def generate(self):
        v = IfVisitor(self.returnToken)
        v.visit(self.body)
        generator = c_generator.CGenerator()
        tests = ""
        if v.testCases == []:
            v.testCases = [[""]]
        for i,t in enumerate(v.testCases):
            tests+= "\nTEST(UT_"+self.testGroup+", "+self.function.decl.name+"_"+str(i)+")\n{"
            tests+= "\n\t//Set input\n"
            tests+= self.getSetup(t)
            tests+= "\n\t//Call FUT\n"
            tests+= self.getFutCall()
            tests+= "\n\t//Check output\n"
            check = self.getCheck(t)
            if check is not None:
                tests+= check
            tests+= "}\n"
        return tests
    
    def getSetup(self,conditions):
        res=""
        # Generate input parameters
        p = ParamDeclVisitor()
        p.visit(self.params)
        res+=p.result;
        # Generate conditions for given UT
        for c in conditions:
            if type(c) is not Return:
                res+="\t//"+str(c)+"\n"
        return res

    def getCheck(self,conditions):
        if conditions is not [] and type(conditions[-1]) is Return:
            check = "\t"+str(conditions[-1])+"\n"
            return check

    def getFutCall(self):
        res = "\t"
        if self.function.decl.type.type.type.names[0] != "void":
            if(type(self.function.decl.type.type.type) is c_ast.TypeDecl):
                res += self.function.decl.type.type.type.type.names[0] + " result = "
            else:
                res += self.function.decl.type.type.type.names[0] + " result = "
        res += self.function.decl.name+"("
        p = ParamVisitor()
        p.visit(self.params)
        res+=", ".join(p.names);
        return res+");\n"
